# Wiki Generator Mod
A mod to generate tables and infoboxes for the wiki, directly from the game source. The mod hooks into built-in events and automatically pulls the information, formats it, and writes it to a text file.

## Usage
Copy this folder to your Isleward server's mod directory (`src/server/mods`).
Run Isleward as usual.

The mod has no dependencies (it only uses `fs`, a built-in Node module, and the Isleward source), so nothing else needs setting up.

Generated files are placed in the `/output` directory of the mod.
The files are generated synchronously, and may take a few seconds to create and write to the file system.
There may be intended slowdowns (with `setTimeout`) to allow other mods to load in new classes/skins/spells before the wiki markup is generated.

## Contributing
Code is very messy.
Pull requests welcome, but touch the code at your own risk!
